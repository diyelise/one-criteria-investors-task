﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace invtask
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            wind.Title = "Решение Задачи Инвестора v.1.0";
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        }

        public string InvPlane = "";
        public int F2X = 0;
        public List<int> ignore_pos = new List<int>();

        private void Author_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Версия сборки: " + Environment.Version.Build.ToString() + "\r\n" + "Автор: Елисеенко Дмитрий, студент магистратуры ИТз1841" + "\r\n", "О программе");
        }

        /// <summary>
        /// Загрузка данных для тестового расчета
        /// </summary>
        void SetDefault()
        {
            O_mas.Text = "1,2,3,4,5,6,7";
            a_mas.Text = "2,2,2,4,2,2,3";
            D_mas.Text = "2,4,7,4,6,8,2";
            T_mas.Text = "4,1,3,20,1,4,6";
        }

        /// <summary>
        /// Запуск расчета и упорядочивания объектов инвестирования
        /// </summary>
        void Calc()
        {
            ignore_pos.Clear();
            F2X = 0;
            int time_end_develop = 0; // Время окончания строительства всех объектов
            report.Text = "";
            
            // Инициализация объектов хранения данных
            Dictionary<int, int> D_data = new Dictionary<int, int>();
            Dictionary<int, int> a_data = new Dictionary<int, int>();
            Dictionary<int, int> T_data = new Dictionary<int, int>();

            int cnt_d = 0; // счетчик
            foreach (int D_val in D_mas.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)))
            {
                D_data.Add(cnt_d + 1, D_val);
                cnt_d++;
            }
            cnt_d = 0;
            foreach (int a_val in a_mas.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)))
            {
                a_data.Add(cnt_d + 1, a_val);
                cnt_d++;
            }
            cnt_d = 0;
            foreach (int T_val in T_mas.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)))
            {
                T_data.Add(cnt_d + 1, T_val);
                cnt_d++;
            }
            cnt_d = 0;

            time_end_develop = T_data.Sum(x => x.Value); // получаем время

            int exec_obj = 0; // счетчик обработанных объектов
            int step_cnt = 1; // шаги
            int minTime = time_end_develop; // Показатель времени Т
            int size_T = T_data.Count;

            do
            {
                report.Text = report.Text + "Шаг: " + step_cnt + "\r\n";
                int out_pos = PrepareRecalc(D_data, a_data, minTime);
                minTime = minTime - T_data[out_pos];
                ignore_pos.Add(out_pos);
                step_cnt++;
                exec_obj++;
            }
            while (exec_obj < size_T);
            ignore_pos.Reverse(); // Формируем обратный порядок
            string invest_order = "";
            foreach (var item in ignore_pos)
            {
                invest_order = invest_order + item + " ";
            }
            SetMatrixData();
            time_end_dev.Content = time_end_develop;
            target_val_func.Content = F2X;
            order_invest_obj.Content = invest_order;
        }

        /// <summary>
        /// Визуализация матрицы данных
        /// </summary>
        void SetMatrixData()
        {
            List<MatrixItem> m_item = new List<MatrixItem>();
            List<int> a = a_mas.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)).ToList();
            List<int> D = D_mas.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)).ToList();
            List<int> T = T_mas.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)).ToList();
            List<int> O = O_mas.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)).ToList();

            int cnt = 0;
            foreach (int O_i in O)
            {
                m_item.Add(new MatrixItem(O_i, a[cnt], T[cnt], D[cnt]));
                cnt++;
            }
            dgrid.ItemsSource = m_item;
        }

        /// <summary>
        /// Класс для визуального отображения
        /// </summary>
        public class MatrixItem
        {
            public int O { get; set; }
            public int a { get; set; }
            public int T { get; set; }
            public int D { get; set; }

            public MatrixItem(int O, int a, int T, int D)
            {
                this.O = O;
                this.a = a;
                this.T = T;
                this.D = D;
            }
        }

        /// <summary>
        /// Предрасчет данных
        /// </summary>
        /// <param name="D"></param>
        /// <param name="a"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        int PrepareRecalc(Dictionary<int, int> D, Dictionary<int, int> a, int time)
        {
            Dictionary<int, int> az = new Dictionary<int, int>();
            Dictionary<int, int> ts = new Dictionary<int, int>();
            int cnt = 1;
            foreach (var d in D)
            {
                if (!ignore_pos.Contains(d.Key))
                {
                    ts.Add(cnt, time - d.Value);
                    az.Add(cnt, (time - d.Value) * a[cnt]);
                    report.Text = report.Text + "z(" + cnt + ") = max(" + time + "-" + d.Value + ") = " + (time - d.Value) + "              a(" + cnt + ")z(" + cnt + ") = " + (time - d.Value) + "*" + a[cnt] + " = " + (time - d.Value) * a[cnt] + "\r\n";
                }
                cnt++;
            }
            if (F2X == 0)
            {
                F2X = (int)az.Select(item => Convert.ToDecimal(item.Value)).Max();
            }
            int minValue = (int)ts.Select(item => Convert.ToDecimal(item.Value)).Min();
            int pos = ts.FirstOrDefault(x => x.Value == minValue).Key;
            report.Text = report.Text + "Мин.знач: " + minValue + "; Для время Т: " + time + "\r\n";
            report.Text = report.Text + "Объект: " + pos + "\r\n" + "-------------------------------------" + "\r\n";
            return pos;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (IsCorrect(O_mas) && IsCorrect(a_mas) && IsCorrect(T_mas) && IsCorrect(D_mas)) // первая проверка
            {
                if (IsDependences()) // вторая проверка
                {
                    try
                    {
                        Calc(); // расчет
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Нарушена количественная зависимость данных i=a=T=d. Проверьте и повторите попытку", "Ошибка нарушения количественной зависимости");
                }
            }
            else
            {
                MessageBox.Show("Проверка на корректность не пройдена. Пожалуйста, проверьте заполненные вами данные", "Ошибка корректности");
            }
        }

        /// <summary>
        /// Проверка соблюдение количественной зависимости O=a=T=D
        /// </summary>
        /// <returns></returns>
        bool IsDependences()
        {
            int obj_count = O_mas.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Count();
            if (a_mas.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Count() == obj_count &&
                T_mas.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Count() == obj_count &&
                D_mas.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Count() == obj_count)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SetDefault();
            invest_object_count.Content = "Объектов инвестирования: " + O_mas.Text.Split(',').Length;
        }

        /// <summary>
        /// Проверка корректности
        /// </summary>
        bool IsCorrect(TextBox t)
        {
            var clear_data = t.Text.Split(',').Where(x => !string.IsNullOrWhiteSpace(x));
            var data = t.Text.Split(',');
            if (clear_data.Count() != data.Count())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Проверка и запрет ввода запятых подряд
        /// </summary>
        /// <param name="t"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        bool IsDeniedRepeat(TextBox t, TextCompositionEventArgs e)
        {
            if (t.Text.Length != 0)
            {
                if (t.Text.Last() == ',' && e.Text == ",")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private void a_mas_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !((Char.IsNumber(e.Text, 0) || e.Text == ",") && !IsDeniedRepeat(a_mas, e));
        }

        private void O_mas_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !((Char.IsNumber(e.Text, 0) || e.Text == ",") && !IsDeniedRepeat(O_mas, e));
        }

        private void T_mas_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !((Char.IsNumber(e.Text, 0) || e.Text == ",") && !IsDeniedRepeat(T_mas, e));
        }

        private void D_mas_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !((Char.IsNumber(e.Text, 0) || e.Text == ",") && !IsDeniedRepeat(D_mas, e));
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            O_mas.Text = "";
            T_mas.Text = "";
            a_mas.Text = "";
            D_mas.Text = "";
        }
    }
}
